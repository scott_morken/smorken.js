/**
 * Created by smorken on 4/21/15.
 * @link http://larrysteinle.com/2013/08/17/how-to-create-a-clean-extensible-javascript-library/
 */
(function (wHnd) {
    var _CreateNS = function (n, t) {
        if (!t) t = wHnd; //Ensure that we have a target object to add the namespace to.
        var nsItem, nsList = n.split("."); //Break the namespace into parts
        for (var i = 0, l = nsList.length; i < l; i++) {
            nsItem = nsList[i]; //Get the next available name part from the namespace
            if (!t[nsItem]) t[nsItem] = Object.create(null); //Add the name part when missing from the namespace
            t = t[nsItem]; //Select the new namespace child
        }
        return t; //Return the leaf namespace part to the caller
    };
    var _Extend = function (n, o, t) {
        if (!t) t = wHnd; //Ensure that we have a target object to add the namespace to.
        if (n.indexOf(".") >= 0) {
            var t2 = n.substr(0, n.lastIndexOf(".")); //Extract the namespace from the name.
            t = _CreateNS(t2, t); //Append the namespace to the target getting a reference to the new namespace leaf child
            n = n.substr(n.lastIndexOf(".") + 1); //Remove the namespace from the name.
        }

        //Add the new behavior, property or object to the target namespace or target object.
        if (n && t.prototype && !t.hasOwnProperty[n]) t.prototype[n] = o; //Add the member for public access (when supported)
        if (n && !t[n]) t[n] = o; //Add the member for static access
    };
    _Extend("smorken.CreateNS", _CreateNS);
    _Extend("smorken.Extend", _Extend);
})(typeof window !== 'undefined' ? window : global);
(function(wHnd) {
    smorken.Extend("smorken.each", function (obj, fn) {
        if (obj.length) for (var i = 0, ol = obj.length, v = obj[0]; i < ol && fn(i, v) !== false; v = obj[++i]);
        else for (var p in obj) if (fn(p, obj[p]) === false) break;
    });
    smorken.Extend("smorken.ready", function(fn) {
        if (document.readyState != 'loading') {
            fn();
        } else {
            document.addEventListener('DOMContentLoaded', fn);
        }
    });
})(typeof window !== 'undefined' ? window : global);

/**
 * Created by smorken on 4/21/15.
 */

(function(wHnd) {
    var event = {
        trigger: function (on, type, data) {
            var event = document.createEvent('CustomEvent');
            event.initEvent(type, true, true);
            event.data = data || {};
            event.eventName = type;
            event.target = on;
            on.dispatchEvent(event);
        },
        bind: function (el, ev, fn) {
            if (window.addEventListener) { // modern browsers including IE9+
                el.addEventListener(ev, fn, false);
            } else if (window.attachEvent) { // IE8 and below
                el.attachEvent('on' + ev, fn);
            } else {
                el['on' + ev] = fn;
            }
        },

        unbind: function (el, ev, fn) {
            if (window.removeEventListener) {
                el.removeEventListener(ev, fn, false);
            } else if (window.detachEvent) {
                el.detachEvent('on' + ev, fn);
            } else {
                elem['on' + ev] = null;
            }
        },
        stop: function (ev) {
            var e = ev || window.event;
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
        }
    };
    smorken.Extend('smorken.Event', event);

})(typeof window !== 'undefined' ? window : global);
