/**
 * Created by smorken on 4/29/15.
 */
QUnit.test("domReady.event", function(assert) {
    assert.expect(1);
    var dispatch = function(on, type) {
        var event = document.createEvent('HTMLEvents');
        event.initEvent(type, true, true);
        event.eventName = type;
        event.target = on;
        on.dispatchEvent(event);
    };
    smorken.ready(function() {
        assert.ok(true, "loaded");
    });
    dispatch(document, 'DOMContentLoaded');
});
QUnit.test('each.array', function(assert) {
    assert.expect(2);
    var test = ['foo', 'bar'];
    smorken.each(test, function(i, k) {
        assert.ok(true, i + ": " + k);
    });
});
QUnit.test('each.object', function(assert) {
    assert.expect(2);
    var test = { foo: 'bar', baz: 'biz' };
    smorken.each(test, function(i, k) {
        assert.ok(true, i + ": " + k);
    });
});