/**
 * Created by smorken on 4/21/15.
 */

(function(wHnd) {
    var event = {
        trigger: function (on, type, data) {
            var event = document.createEvent('CustomEvent');
            event.initEvent(type, true, true);
            event.data = data || {};
            event.eventName = type;
            event.target = on;
            on.dispatchEvent(event);
        },
        bind: function (el, ev, fn) {
            if (window.addEventListener) { // modern browsers including IE9+
                el.addEventListener(ev, fn, false);
            } else if (window.attachEvent) { // IE8 and below
                el.attachEvent('on' + ev, fn);
            } else {
                el['on' + ev] = fn;
            }
        },

        unbind: function (el, ev, fn) {
            if (window.removeEventListener) {
                el.removeEventListener(ev, fn, false);
            } else if (window.detachEvent) {
                el.detachEvent('on' + ev, fn);
            } else {
                elem['on' + ev] = null;
            }
        },
        stop: function (ev) {
            var e = ev || window.event;
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
        }
    };
    smorken.Extend('smorken.Event', event);

})(typeof window !== 'undefined' ? window : global);
