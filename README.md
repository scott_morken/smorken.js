### Bower Install

```
{
  "name": "my-project",
  "dependencies": {
    "scroll.js": "git@bitbucket.org:scott_morken/scroll.js.git#~0.0.1"
  }
}
```

### Gulpfile addition

```
...
  .copy('./bower_components/scroll.js/dist', './public/js/limited')
...
```